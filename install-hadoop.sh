
#
# Copyright © 2020 Maestro Creativescape
#
# SPDX-License-Identifier: GPL-3.0
#
# Install Helper for Hadoop on Docker
# 

echo "Fetching Hadoop... Please wait...."
curl -sLo hadoop-2.7.3.tar.gz https://archive.apache.org/dist/hadoop/core/hadoop-2.7.3/hadoop-2.7.3.tar.gz
echo "Extracting and stripping unnecessary folders....."
tar -xf hadoop-2.7.3.tar.gz hadoop-2.7.3/share/hadoop/
rm -rf hadoop-2.7.3.tar.gz
echo "Please use folder hadoop in current directory instead of /usr/local/hadoop in the guide, for adding external JAR"
echo "Pulling Docker Image, please wait...."
docker pull registry.gitlab.com/baalajimaestro/hadoop-docker
echo "Please mount your eclipse workspace for the /hadoop/workspace folder"
echo "How to run this:"
echo "docker run -it -v $HOME/eclipse-workspace:/hadoop/workspace -p 50070:50070 registry.gitlab.com/baalajimaestro/hadoop-docker"
echo "Docker runs as root (UID 0) and files made by docker container may not be readable by host"