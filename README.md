# Hadoop Docker

- Image with hadoop 2.7.3 with OpenJDK 1.8.0 running under Alpine Linux Edge.
- Needs Docker For Linux/Windows(Needs WSL2 Engine).
- Bind mount the folder for your eclipse workspace to /hadoop/workspace.
- Run the provided install-hadoop.sh for getting the basic install done. (It pulls the necessary jar files, and the docker image)
