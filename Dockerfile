
#
# Copyright © 2020 Maestro Creativescape
#
# SPDX-License-Identifier: GPL-3.0
#
# Docker Image Builder for Hadoop
# 

# We are using Alpine
FROM alpine:edge

# Deps
RUN apk update && \
    echo "Installing Dependencies!" && \
    apk add openjdk8 \
            which \
            bash \
            openssh \
            curl \
            openssh-client \
            openssh-server  --no-cache --update > /dev/null && \
    echo "Downloading and Extracting Hadoop!" && \
    curl -sLo hadoop-2.7.3.tar.gz https://archive.apache.org/dist/hadoop/core/hadoop-2.7.3/hadoop-2.7.3.tar.gz && \
    tar -xf hadoop-2.7.3.tar.gz && \
    mv hadoop-2.7.3 /hadoop && \
    rm -rf hadoop-2.7.3.tar.gz   

# Prepare SSH and Node directories
RUN mkdir -p /hadoop_store/hdfs/namenode && mkdir -p /hadoop_store/hdfs/datanode && mkdir -p /app/hadoop/tmp
RUN ssh-keygen -A && ssh-keygen -b 2048 -t rsa -f /root/.ssh/id_rsa -q -N "" && cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys && chmod 600 /root/.ssh/authorized_keys

# Copy our custom configs
COPY core-site.xml /hadoop/etc/hadoop/
COPY mapred-site.xml /hadoop/etc/hadoop/
COPY hdfs-site.xml /hadoop/etc/hadoop/
COPY log4j.properties /hadoop/etc/hadoop/

# Enviroment Variables
ENV HADOOP_INSTALL /hadoop
ENV HADOOP_MAPRED_HOME /hadoop
ENV HADOOP_COMMON_HOME /hadoop
ENV HADOOP_CONF_DIR /hadoop/etc/hadoop
ENV HADOOP_ROOT_LOGGER "WARN,DRFA"
ENV HADOOP_HOME_WARN_SUPPRESS 1
ENV YARN_IDENT_STRING root
ENV HADOOP_HDFS_HOME /hadoop
ENV YARN_HOME /hadoop
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ARG JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk
ENV HADOOP_COMMON_LIB_NATIVE_DIR /hadoop/lib/native
ENV HADOOP_OPTS "-Djava.library.path=/hadoop/lib"

# Set Paths and PS1
ENV PATH /hadoop/bin:/hadoop/sbin:$PATH
ENV PS1 '[[container] \u@\h \W]\$ '

# Setup the entrypoint which will start nodes for hadoop whenever the container is started
RUN printf '#! /bin/sh \n\
/usr/sbin/sshd -D & \n\
/hadoop/bin/hdfs --config $HADOOP_CONF_DIR namenode &\n\
/hadoop/bin/hdfs --config $HADOOP_CONF_DIR secondarynamenode &\n\
/hadoop/bin/yarn --config $HADOOP_CONF_DIR nodemanager &\n\
/hadoop/bin/yarn --config $HADOOP_CONF_DIR resourcemanager &\n\
/hadoop/bin/hdfs --config $HADOOP_CONF_DIR datanode &\n' > /root/entrypoint-hadoop.sh

RUN printf '#! /bin/sh \n\
printf "\033c" \n\
echo "Starting Hadoop Services... Please give me a moment!" \n\
bash /root/entrypoint-hadoop.sh &> /dev/null & \
clear \n\
echo "*******************************************" \n\
echo "Welcome to baalajimaestro hadoop container!" \n\
echo "This Container is powered by Alpine Linux $(cat /etc/alpine-release)" \n\
echo "You are running Java Version $(java -version 2>&1 | head -1 | cut -d\" -f2)" \n\
echo "You are using Hadoop $(hadoop version 2>&1 | head -n 1 |  cut -d" " -f2)" \n\
echo "Your files from your mount will be available at /hadoop/workspace, please quit and bind-mount if you havent" \n\
echo "Have a great day!" \n\
echo "*******************************************" \n\
echo "" \n\
echo "" \n\
/bin/bash \n' > /root/entrypoint.sh && chmod +x /root/entrypoint.sh

# Wipe namenode fs
RUN echo "Wiping NameNode for 1st Run" && /hadoop/bin/hadoop namenode -format > /dev/null

# UI, accessible on a browser.
EXPOSE 50070

#Persistent Volumes
VOLUME ["/hadoop_store/hdfs/namenode", "/hadoop_store/hdfs/datanode", "/app/hadoop/tmp", "/hadoop/workspace"]

ENTRYPOINT [ "/root/entrypoint.sh" ]